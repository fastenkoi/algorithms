package sort;

import org.apache.log4j.Level;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class SortTest {
    private static final int RANDOM_ARRAY_MAX_LENGTH = 10000;
    private static final int RANDOM_ARRAY_MAX_VALUE = 100000;

    private int[] ascArray;
    private int[] descArray;
    private int[] mixedArray;
    private final int[] sortedArray = new int[]{1,2,3,4,5,6,7,8,9,10};
    private final int[] randomArray = SortUtil.createArray(RANDOM_ARRAY_MAX_LENGTH, RANDOM_ARRAY_MAX_VALUE);

    private void init() {
        ascArray = new int[]{1,2,3,4,5,6,7,8,9,10};
        descArray = new int[]{10,9,8,7,6,5,4,3,2,1};
        mixedArray = new int[]{2,8,1,6,10,9,4,7,3,5};
    }

    @Before
    public void setUp() {
        AbstractSort.LOG.setLevel(Level.OFF);
    }

    @Test
    public void testBubbleSort() {
        testAlgorithm(new BubbleSort());
    }

    @Test
    public void testShakerSort() {
        testAlgorithm(new ShakerSort());
    }

    @Test
    public void testStrightInsertionSort() {
        testAlgorithm(new StrightInsertionSort());
    }

    @Test
    public void testBinaryInsertionSort() {
        testAlgorithm(new BinaryInsertionSort());
    }

    @Test
    public void testStrightSelectionSort() {
        testAlgorithm(new StrightSelectionSort());
    }

    @Test
    public void testShellSort() {
        testAlgorithm(new ShellSort());
    }

    private void testAlgorithm(Sort algorithm) {
        init();

        algorithm.sort(ascArray);
        Assert.assertArrayEquals(ascArray, sortedArray);

        algorithm.sort(descArray);
        Assert.assertArrayEquals(descArray, sortedArray);

        algorithm.sort(mixedArray);
        Assert.assertArrayEquals(mixedArray, sortedArray);

        int[] a = randomArray.clone();
        int[] b = randomArray.clone();
        algorithm.sort(a);
        Arrays.sort(b);
        Assert.assertArrayEquals(a, b);
    }
}


