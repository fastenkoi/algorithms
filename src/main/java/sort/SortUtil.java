package sort;

import java.util.Random;

public class SortUtil {
    public static int[] createArray(int length, int maxValue) {
        Random generator = new Random();

        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = generator.nextInt(maxValue);
        }

        return array;
    }
}
