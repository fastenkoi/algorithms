package sort;

public class ShellSort extends AbstractSort {
    private int[] gaps;

    protected void beforeSort(int[] a) {
        if (a.length <= 4000) {
            gaps = new int[]{1,4,10,23,57,132,301,701,1750};
        } else {
            gaps = new int[]{1};
            int i = 1;
            boolean even = false;
            while (3 * gaps[i-1] < a.length) {
                int[] newGaps = new int[gaps.length + 1];
                System.arraycopy(gaps, 0, newGaps, 0, gaps.length);
                gaps = newGaps;
                if (even) {
                    gaps[i] = 9 * (int)Math.pow(2, i) - 9 * (int)Math.pow(2, i/2) + 1;
                } else {
                    gaps[i] = 8 * (int)Math.pow(2, i) - 6 * (int)Math.pow(2, (i+1)/2) + 1;
                }
                even = !even;
                i++;
            }
        }
    }

    protected void doSort(int[] a) {
        int element, j;
        for (int m = gaps.length - 1; m >= 0; m--) {
            int gap = gaps[m];
            for (int i = gap; i <= a.length - 1; i += gap) {
                element = a[i];
                j = i - gap;
                while (j >= gap && element < a[j]) {
                    a[j + gap] = a[j];
                    j = j - gap;
                }
                if (j >= gap || element >= a[j]) {
                    a[j + gap] = element;
                } else {
                    a[j + gap] = a[j];
                    a[j] = element;
                }
            }
        }
    }

    protected String getAlgorithmName() {
        return "SHELL SORT";
    }
}
