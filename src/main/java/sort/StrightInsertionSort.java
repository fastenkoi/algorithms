package sort;

/**
 * Алгоритм сортировки массива вставками
 */
public class StrightInsertionSort extends AbstractSort {
    protected void beforeSort(int[] array) {}

    protected void doSort(int[] a) {
        int current, j;
        //просматриваем ВСЕ элементы массива начиная со второго
        for (int i = 1; i <= a.length - 1; i++) {
            current = a[i];
            j = i;
            //все элементы слева, которые больше текущего
            while (j >= 1 && a[j - 1] > current) {
                //сдвигаем вправо на одну позицию
                a[j] = a[j - 1];
                j--;
            }
            //а текущий элемент вставляем сразу на нужную(освободившуюся) позицию
            a[j] = current;
        }
    }

    protected String getAlgorithmName() {
        return "STRIGHT INSERTION SORT";
    }
}
