package sort;

public class BubbleSort extends AbstractSort {
    protected void beforeSort(int[] array) {}

    protected void doSort(int[] a) {
        int countMoves;
        do {
            countMoves = 0;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i + 1]) {
                    int temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;

                    countMoves++;
                }
            }
        } while (countMoves > 0);
    }

    protected String getAlgorithmName() {
        return "BUBBLE SORT";
    }
}
