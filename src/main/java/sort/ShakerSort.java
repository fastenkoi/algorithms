package sort;

public class ShakerSort extends AbstractSort {
    protected void beforeSort(int[] array) {}

    protected void doSort(int[] a) {
        int countMoves;
        int left = 0, right = a.length - 1;
        do {
            countMoves = 0;
            for (int i = left; i < right; i++) {
                if (a[i] > a[i + 1]) {
                    int temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;

                    countMoves++;
                }
            }
            right--;
            for (int i = right; i > left; i--) {
                if (a[i - 1] > a[i]) {
                    int temp = a[i];
                    a[i] = a[i - 1];
                    a[i - 1] = temp;

                    countMoves++;
                }
            }
            left++;
        } while (left < right && countMoves > 0);
    }

    protected String getAlgorithmName() {
        return "SHAKER SORT";
    }
}
