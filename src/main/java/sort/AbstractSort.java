package sort;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.util.StopWatch;

public abstract class AbstractSort implements Sort {
    static final Logger LOG = LogManager.getLogger(AbstractSort.class);

    public void sort(int[] array) {
        StopWatch stopWatch = new StopWatch(getAlgorithmName());

        stopWatch.start("beforeSort");
        beforeSort(array);
        stopWatch.stop();

        stopWatch.start("doSort");
        doSort(array);
        stopWatch.stop();

        LOG.debug(stopWatch.prettyPrint());
    }

    protected abstract void beforeSort(int[] array);

    protected abstract void doSort(int[] array);

    protected abstract String getAlgorithmName();
}
