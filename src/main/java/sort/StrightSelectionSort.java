package sort;

public class StrightSelectionSort extends AbstractSort {
    protected void beforeSort(int[] array) {}

    protected void doSort(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            int k = i, minValue = a[i];
            for (int j = i + 1; j <= a.length - 1; j++) {
                if (a[j] < minValue) {
                    k = j;
                    minValue = a[j];
                }
            }
            a[k] = a[i];
            a[i] = minValue;
        }
    }

    protected String getAlgorithmName() {
        return "STRIGHT SELECTION SORT";
    }
}
