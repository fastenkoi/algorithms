import sort.*;

import java.util.ArrayList;
import java.util.List;

public class SortClient {
    private final List<Sort> algorithms = new ArrayList<Sort>();

    public SortClient() {
        algorithms.add(new BubbleSort());
        algorithms.add(new ShakerSort());
        algorithms.add(new StrightInsertionSort());
        algorithms.add(new BinaryInsertionSort());
        algorithms.add(new StrightSelectionSort());
        algorithms.add(new ShellSort());
    }

    void sort() {
        int[] array = SortUtil.createArray(4000, 20000);

        for (Sort algorithm : this.algorithms) {
            algorithm.sort(array.clone());
        }
    }
}
